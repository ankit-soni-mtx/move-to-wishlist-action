/**
 * Created By ankitSoni on 05/26/2020
 */
public class DataTableController {
        
    /**
     * Description : Add cart item to the existing wishlist
     * @param : cartId , cartItemId
     * @return : True if successful
     */
    @AuraEnabled
    public static Boolean addCartItemToWishlist(String cartId , String cartItemId){
        String majorProductId = null;
        Boolean wasSuccessful = false;
        Boolean wasMajorSuccessful = false , wasMinorSuccessful = true;

        Boolean removeCartItem = false;
        String productIdToAdd = null;
        
        String currentWishlistId = '';
        String userId = UserInfo.getUserId(); // sfid of the logged in user
        
        List<ccrz__E_CartItem__c> minorCartItemList = new List<ccrz__E_CartItem__c>();
        List<ccrz__E_CartItem__c> inserMinorWishItemList = new List<ccrz__E_CartItem__c>();
        List<String> removeIds = new List<String>();
		Map<String,ccrz__E_CartItem__c> cartItemMap = new Map<String,ccrz__E_CartItem__c>();

        System.debug('addCartItemToWishlist()');

        //get current wishlist of the user
        List<ccrz__E_Cart__c> wishlist = [SELECT Id, ccrz__ActiveCart__c, ccrz__CartType__c,ccrz__User__c
                                        FROM ccrz__E_Cart__c
                                        WHERE ccrz__User__c=: userId
                                        AND ccrz__ActiveCart__c=true
                                        AND ccrz__CartType__c='WishList'
                                        LIMIT 1];
        for(ccrz__E_Cart__c cart: wishlist){
            currentWishlistId = cart.Id;
        }

        if( (cartId != null && cartItemId != null) || (String.isNotBlank(cartId) && String.isNotBlank(cartItemId)) ){
            
            List<ccrz__E_CartItem__c> cartItemList = [
                SELECT Id , ccrz__Product__c ,ccrz__Quantity__c , ccrz__ParentCartItem__c , ccrz__cartItemType__c
                (SELECT Id , ccrz__Product__c  FROM ccrz__Cart_Items__r)
                FROM ccrz__E_CartItem__c 
                WHERE Id =:cartItemId AND ccrz__Cart__c =:cartId];

            for(ccrz__E_CartItem__c cartItem : cartItemList){
                majorProductId = cartItem.ccrz__Product__c;
                minorCartItemList = cartItem.ccrz__Cart_Items__r;
                cartItemMap.put(cartItem.Id, cartItem);
            }       
            
            System.debug('childs-'+minorCartItemList);
            
            //Insert new Major Wishlist Item 
            ccrz__E_CartItem__c newWishlistItem = new ccrz__E_CartItem__c();
            newWishlistItem.ccrz__Cart__c = currentWishlistId;
            newWishlistItem.ccrz__Product__c = majorProductId;
            newWishlistItem.ccrz__cartItemType__c = 'Major';
            newWishlistItem.ccrz__Price__c = 0.00;
            newWishlistItem.ccrz__Quantity__c = 1;
            
            try{
                insert newWishlistItem;
                wasMajorSuccessful = true;
            }
            catch(DmlException dex){
                System.debug('Insert to wishlist from cart exception - '+dex.getMessage());
            }
            
            //Insert Minor Wishlist Items if any

            if(!minorCartItemList.isEmpty()){
                for(ccrz__E_CartItem__c minorCartItem : minorCartItemList){

                    ccrz__E_CartItem__c newMinorItem = new ccrz__E_CartItem__c();
                    newMinorItem.ccrz__Cart__c = currentWishlistId;
                    newMinorItem.ccrz__Product__c = minorCartItem.ccrz__Product__c;
                    newMinorItem.ccrz__cartItemType__c = 'Minor';
                    newMinorItem.ccrz__Price__c = 0.00;
                    newMinorItem.ccrz__Quantity__c = 1;
                    newMinorItem.ccrz__ParentCartItem__c = newWishlistItem.Id; // Newly created Major WishlistItem

                    inserMinorWishItemList.add(newMinorItem);
                }

                if(!inserMinorWishItemList.isEmpty()){
                    try{
                        insert inserMinorWishItemList;
                        wasMinorSuccessful = true;
                    }
                    catch(DmlException dex){
                        wasMinorSuccessful = false;
                        System.debug('Insert minor wishlist item exception - '+dex.getMessage());
                    }
                }

            }
        } 

        // remove items from cart now
        removeIds.add(cartItemId);
        for(ccrz__E_CartItem__c item : minorCartItemList){
            removeIds.add(item.Id);
        }
    
        if(wasMajorSuccessful && wasMinorSuccessful){
            wasSuccessful = true;
            removeCartItem = removeCartItem(cartId,removeIds);
        }

        return wasSuccessful;       
    }

    /**
     * Description : Remove cart item from Cart 
     * @param : cartId , cartItemId
     * @return : True if successful
     */
    @AuraEnabled
    public static Boolean removeCartItem(String cartId , List<String> cartItemIdList){
        Boolean wasSuccessful = false;
        System.debug('removeCartItem()');
        
        List<ccrz.ccApiCart.LineData> deleteCartItemList = new List<ccrz.ccApiCart.LineData>();
   
        if (cartItemIdList != null && cartItemIdList.size() > 0){
            for (String item : cartItemIdList){
                ccrz.ccApiCart.LineData lineItem = new ccrz.ccApiCart.LineData();
                lineItem.sfid = item;
                deleteCartItemList.add(lineItem);
            }
            System.debug('Remoove cart items: ' + deleteCartItemList);
        }

        Map<String, Object> inputData = new Map<String, Object>{
            ccrz.ccAPI.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
            ccrz.ccApiCart.CART_ID => cartId,
            ccrz.ccApiCart.LINE_DATA => deleteCartItemList
        };
           
        try {
            Map<String, Object> outputData = ccrz.ccApiCart.removeFrom(inputData);
            wasSuccessful = (Boolean) outputData.get(ccrz.ccApi.SUCCESS);

        } catch (Exception e) {
            System.debug('Remove cartItem from cart exception - '+e.getMessage());
        }  
        
        return wasSuccessful ;
    }

}
