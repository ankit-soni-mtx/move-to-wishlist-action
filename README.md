## Asset Description :
- This asset contains 1 apex class.
- `addCartItemToWishlist` method moves selected cart item to the current wishlist of the logged in user.
- This has been used in one of the B2B Commerce Cloud project (7s nVent B2B Commerce Cloud Implementation).

## Asset Use :
- This apex utility can be linked to Aura Component as well as Lightning Web Component , just need to pass `Current Card Id` , `Cart Item Id` as the parameter on apex callout from the Lightning component JavaScript handler.